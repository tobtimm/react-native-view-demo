//
//  NativeComponentManager.swift
//  ReactNativeViewDemo
//
//  Created by Tobias Timm on 2018-10-15.
//  Copyright © 2018 Facebook. All rights reserved.
//

import Foundation

@objc (NativeComponentManager)
class NativeComponentManager: RCTViewManager {
  
  override func view() -> UIView! {
    let customLabel = CustomLabel()
    customLabel.textColor = UIColor.blue
    customLabel.textAlignment = NSTextAlignment.center
    return customLabel
  }
  
  @objc func updateValueViaManager(_ node:NSNumber) {
    DispatchQueue.main.async {
      let customLabel = self.bridge.uiManager.view(forReactTag: node) as! CustomLabel
      customLabel.updateValue()
    }
  }
}

class CustomLabel: UILabel {
  
  func updateValue() {
    self.backgroundColor = UIColor.red
    self.customText = "Updated text!"
  }
  
  var _customText : String?
  @objc var customText: String? {
    set {
      _customText = newValue
      self.text = _customText
    }
    get {
      return _customText
    }
  }
}

