//
//  SwiftComponentModule.m
//  MediumSwiftNativeExample
//
//  Created by Tobias Timm on 2018-10-11.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTViewManager.h>

@interface RCT_EXTERN_MODULE(NativeComponentManager, RCTViewManager)

RCT_EXPORT_VIEW_PROPERTY(customText, NSString)

RCT_EXTERN_METHOD(updateValueViaManager:(nonnull NSNumber *)node)

@end
